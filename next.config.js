// /** @type {import('next').NextConfig} */

// const nextConfig = {
//   reactStrictMode: true,
// }

// module.exports = nextConfig

const withLess = require("next-with-less");

module.exports = {
  ...withLess({
    lessLoaderOptions: {      
      lessOptions: {
        modifyVars: {
          "primary-color": "#1b5e41",
          "border-radius-base": "1rem",
        },
      },
    },
  }),
  trainingSlash: true,
  images: {
    loader: 'akamai',
    path: '/'
  }
};
