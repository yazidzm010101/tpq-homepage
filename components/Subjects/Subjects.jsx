import { Avatar, Button, Card, List, Modal, Skeleton, Typography, Upload } from "antd";

import {
  faExclamationCircle,
  faX,  
  faRotate,
  faDoorOpen
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import { charToColor, chartToPatternStyle } from "../../utils/utils";

const SubjectItem = ({ title, body, onClick }) => {
  const background = charToColor(title);
  return (
    <Card
      size="small"
      className="overflow-hidden border-[rgba(0,0,0,0.025)] hover:bg-gray-100 transition-all duration-300"
      style={{ boxShadow: "inset 0 -1px 0 0 rgba(0, 0, 0, 0.05)" }}
      hoverable
      onClick={onClick}
      draggable
    >
      <div className="-mt-3 -mx-3 relative" style={{ background }}>
        <div className="absolute top-0 w-full h-full" style={!!title && chartToPatternStyle(title)}></div>
        <Typography.Title
          ellipsis={{ rows: 1 }}
          level={3}          
          className="relative text-center pt-11 pb-12 px-6 text-white"
        >
          {title}
        </Typography.Title>        
      </div>
      <Typography.Paragraph className="text-xs" ellipsis={{ rows: 2 }}>
        {body}
      </Typography.Paragraph>
    </Card>
  );
};

const SubjectDetail = ({
  visible = false,
  createdAt,
  title,  
  body,
  filePath,
  fileName,
  onCancel,
}) => {
  const [fileList, setFileList] = React.useState()
  React.useEffect(() => {
    if (visible) {
      setFileList([
        {          
          name: fileName,
          status: 'done',
          url: filePath,
        }])
      console.log(fileList)
    } else {
      setFileList(null)
    }
  }, [visible])
  return (
    <Modal
      className="colorized-modal"
      visible={visible}
      onCancel={onCancel}
      title={
        <div
          className="flex relative overflow-hidden justify-center rounded-t-xl text-white -mx-6 -my-9 px-6 py-7 min-h-[10rem] items-center"
          style={{
            background: !!title && charToColor(title),
          }}
        >          
          <div className="absolute top-0 w-full h-full" style={!!title && chartToPatternStyle(title)}></div>
          <div className="relative text-center px-4 overflow-x-hidden">
            <Typography.Title            
              level={2}              
              ellipsis={{rows: 2}}
              className="text-white leading-none mb-0 max-w-full"
            >
              {title}
            </Typography.Title>            
          </div>          
        </div>
      }
      footer={null}
    >
      <Typography.Paragraph className="min-h-[30vh] py-4">
        {body}
      </Typography.Paragraph>
      {
        fileList && <Upload            
          disabled
          listType="picture"            
          maxCount={1}
          defaultFileList={fileList}
          className="w-full mt-4 text-right"          
        />
      }      
    </Modal>
  );
};

const Subjects = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 3,
  xl = 4,
  xxl = 4,
  pageSize = 12,
  loading,
  error,
}) => {
  const [detail, setDetail] = React.useState();
  if (!!loading) {
    data = Array.from(Array(10).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          className="text-primary-custom h-16"
        />
        <h5 className="mb-0 text-primary-custom">
          Belum ada data terbaru saat ini
        </h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item>
            <SubjectItem
              onClick={() => setDetail(item)}
              createdAt={item.createdAt}
              title={item.title}
              filePath={item.filePath}
              body={item.description}
            />
          </List.Item>
        )}
      />
      <SubjectDetail
        visible={!!detail}        
        title={detail && detail.title}
        body={detail && detail.description}
        fileName={!!detail && detail.fileName}
        filePath={detail && detail.filePath}
        onCancel={() => setDetail(null)}
      />
    </>
  );
};

export default Subjects;
