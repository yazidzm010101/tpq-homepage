import { Avatar, Button, Card, List, Modal, notification, Skeleton, Typography, Upload } from "antd";

import {
  faExclamationCircle,
  faX,  
  faRotate,
  faDoorOpen,
  faUpload,
  faSave,
  faTrash
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import { charToColor, chartToPatternStyle } from "../../utils/utils";
import TextArea from "antd/lib/input/TextArea";
import { useDocument } from "swr-firestore-v9";
import firebaseUploader from "../../utils/firebaseUpload";

const SubjectDelete = ({ id, visible = false, onCancel, onDeleted }) => {
  const { deleteDocument } = useDocument("learning_center/" + id);
  const [ loading, setLoading ] = React.useState(false)
  const handleSubmit = async () => {
    try {
      setLoading(true)
      await deleteDocument()
      setLoading(false)      
      notification.info({
        message: 'Sukses',
        description: 'Data berhasil dihapus!',
        placement: 'bottomRight'
      })
      onCancel()
      onDeleted()
    } catch (err) {
      setLoading(false)      
      notification.error({
        message: 'Peringatan',
        description: 'Gagal menghapus data, silahkan coba beberapa saat lagi!',
        placement: 'bottomRight'
      })
    }

  }
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={"Hapus data"}
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faTrash}
            />
          }
          key="close"
          type="secondary"
          onClick={onCancel}
        >
          Tutup
        </Button>,
        <Button
          loading={loading}
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faTrash}
            />
          }
          key="close"
          onClick={handleSubmit}
          type="danger"
        >
          Hapus
        </Button>,
      ]}
    >
      Anda yakin ingin menghapus data ini?
    </Modal>
  );
};

const SubjectItem = ({ title, description, onClick }) => {
  const background = charToColor(title);
  return (
    <Card
      size="small"
      className="overflow-hidden border-[rgba(0,0,0,0.025)] hover:bg-gray-100 transition-all duration-300"
      style={{ boxShadow: "inset 0 -1px 0 0 rgba(0, 0, 0, 0.05)" }}
      hoverable
      onClick={onClick}
      draggable
    >
      <div className="-mt-3 -mx-3 relative" style={{ background }}>
        <div className="absolute top-0 w-full h-full" style={!!title && chartToPatternStyle(title)}></div>
        <Typography.Title
          ellipsis={{ rows: 1 }}
          level={3}          
          className="relative text-center pt-11 pb-12 px-6 text-white"
        >
          {title}
        </Typography.Title>        
      </div>
      <Typography.Paragraph className="text-xs" ellipsis={{ rows: 2 }}>
        {description}
      </Typography.Paragraph>
    </Card>
  );
};

const SubjectDetail = ({
  visible = false,  
  title,
  id,
  description,
  filePath,
  fileName,
  onChange,
  onCancel,
}) => {
  const [fileList, setFileList] = React.useState()
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [uploading, setUploading] = React.useState(false); 
  const [deleteShown, setDeleteShown] = React.useState(false);
  const { update } = useDocument("learning_center/" + id);
  const fbUploader = firebaseUploader("learning_center");
  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,      
    });
    console.log(e)
    onChange({ [e.target.name]: e.target.value });
  };
  const handleUploadChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
    }
    if (info.file.status === "done") {            
      setForm({ ...form, filePath: info.file.response, fileName: info.file.name });
      onChange({ filePath: info.file.response, fileName: info.file.name });
      setUploading(false);
      console.log(form);
    }
    console.log(info);
  };
  const handleSubmit = async () => {
    try {
      setLoading(true);
      await update(form);
      setLoading(false);
      notification.info({
        message: "Sukses",
        description: "Data berhasil disimpan!",
        placement: "bottomRight",
      });
      onCancel();
    } catch (err) {
      setLoading(false);      
      notification.error({
        message: "Peringatan",
        description:
          "Gagal menyimpan perubahan, silahkan coba beberapa saat lagi!",
        placement: "bottomRight",
      });
    }
  };
  React.useEffect(() => {
    if (visible) {
      setFileList([
        {          
          name: fileName,
          status: 'done',
          url: filePath,
        }])
      console.log(fileList)
    } else {
      setFileList(null)
    }
  }, [visible])
  return (
    <>
      <Modal
        className="colorized-modal"
        visible={visible}
        onCancel={onCancel}
        title={
          <div
            className="flex relative overflow-hidden justify-center rounded-t-xl text-white -mx-6 -my-9 px-6 py-7 min-h-[10rem] items-center"
            style={{
              background: !!title && charToColor(title) || charToColor('a'),
            }}
          >          
            <div className="absolute top-0 w-full h-full" style={!!title && chartToPatternStyle(title) || chartToPatternStyle('a')}></div>
            <div className="relative px-4 w-full">
              <TextArea
                level={2}  
                autoSize
                showCount
                size="large"              
                maxLength={64}      
                value={title}   
                name="title"
                onChange={handleChange}                          
                className="transparent text-center children:bg-opacity-20 bg-opacity-0 border-transparent leading-none mb-0 w-full"
              />            
            </div>          
          </div>
        }
        footer={[
          <Button
            icon={
              <FontAwesomeIcon
                className="h-3 mb-0.5 mr-1 inline-block"
                icon={faTrash}
              />
            }
            key="trash"
            type="secondary"
            className="text-red-500 hover:border-red-500"
            onClick={() => setDeleteShown(true)}
          >
            Hapus
          </Button>,
          <Button
            loading={loading}
            disabled={uploading}
            onClick={handleSubmit}
            icon={
              <FontAwesomeIcon
                className="h-3 mb-0.5 mr-1 inline-block"
                icon={faSave}
              />
            }
            key="open"
            type="primary"
          >
            Simpan
          </Button>,
        ]}
      >
        <TextArea name="description" className="min-h-[30vh] mt-4 p-4" autoSize value={description} onChange={handleChange}/>      
        {
          fileList && <Upload            
            onChange={handleUploadChange}            
            listType="picture"            
            maxCount={1}
            defaultFileList={fileList}
            className="w-full mt-4 text-right"          
            customRequest={(e) => fbUploader(e)}
          >
            <Button icon={<FontAwesomeIcon className="w-4 h-4 mr-3 inline-block" icon={faUpload} />}>Unggah dokumen (Max: 1)</Button>
          </Upload>
        }      
      </Modal>
      <SubjectDelete id={id} visible={deleteShown} onCancel={() => setDeleteShown(false)} onDeleted={() => onCancel()}/>
    </>
  );
};

const SubjectManager = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 3,
  xl = 4,
  xxl = 4,
  pageSize = 12,
  loading,
  error,
}) => {
  const [detail, setDetail] = React.useState();
  if (!!loading) {
    data = Array.from(Array(10).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          className="text-primary-custom h-16"
        />
        <h5 className="mb-0 text-primary-custom">
          Belum ada data terbaru saat ini
        </h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item>
            <SubjectItem
              onClick={() => setDetail(item)}
              createdAt={item.createdAt}
              title={item.title}
              filePath={item.filePath}
              description={item.description}
            />
          </List.Item>
        )}
      />
      <SubjectDetail
        visible={!!detail}        
        id={!!detail && detail.id}
        title={!!detail && detail.title}
        description={!!detail && detail.description}
        filePath={!!detail && detail.filePath}
        fileName={!!detail && detail.fileName}
        onChange={(newDetail) => setDetail({ ...detail, ...newDetail })}
        onCancel={() => setDetail(null)}        
      />
    </>
  );
};

export default SubjectManager;
