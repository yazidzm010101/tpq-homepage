import {
  Avatar,
  Button,
  Card,
  List,
  Modal,
  notification,
  Skeleton,
  Typography,
  Upload,
} from "antd";

import {
  faExclamationCircle,
  faX,
  faRotate,
  faDoorOpen,
  faUpload,
  faSave,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import { charToColor, chartToPatternStyle } from "../../utils/utils";
import TextArea from "antd/lib/input/TextArea";
import { useDocument } from "swr-firestore-v9";
import firebaseUploader from "../../utils/firebaseUpload";

const SubjectCreate = ({ visible = false, onSubmit, onCancel }) => {
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [uploading, setUploading] = React.useState(false);
  const fbUploader = firebaseUploader("learning_center");  
  const [fileList, setFileList] = React.useState()
  const handleSubmit = async () => {
    try {
      setLoading(true);
      await onSubmit(form);
      setLoading(false);
      notification.info({
        message: "Sukses",
        description: "Data berhasil ditambah!",
        placement: "bottomRight",
      });
      setForm({});
      onCancel();
    } catch (err) {
      notification.error({
        message: "Peringatan",
        description:
          "Gagal menambahkan data, silahkan coba beberapa saat lagi!",
        placement: "bottomRight",
      });
    }
  };
  const handleChange = (e) => {
    setForm({
      ...form,
      createdAt: new Date().getTime(),
      [e.target.name]: e.target.value,
    });        
  };
  const handleUploadChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
    }
    if (info.file.status === "done") {
      setForm({
        ...form,
        createdAt: new Date().getTime(),
        filePath: info.file.response,
        fileName: info.file.name,
      });      
      setUploading(false);
      console.log(form);
    }
    console.log(info);
  };
  const handleCancel = () => {
    setForm({});
    onCancel();
  };
  React.useEffect(() => {    
    if (visible) {
      setFileList([])    
    } else {
      setFileList(null)
    }
  }, [visible])
  return (
    <Modal
      className="colorized-modal"
      visible={visible}
      onCancel={handleCancel}
      title={
        <div
          className="flex relative overflow-hidden justify-center rounded-t-xl text-white -mx-6 -my-9 px-6 py-7 min-h-[10rem] items-center"
          style={{
            background: !!form.title && charToColor(form.title) || charToColor('a'),
          }}
        >
          <div
            className="absolute top-0 w-full h-full"
            style={
              (!!form.title && chartToPatternStyle(form.title)) ||
              chartToPatternStyle("a")
            }
          ></div>
          <div className="relative px-4 w-full">
            <TextArea
              level={2}
              autoSize
              showCount
              size="large"
              maxLength={64}
              name="title"
              onChange={handleChange}
              value={form.title}
              className="transparent text-center children:bg-opacity-20 bg-opacity-0 border-transparent leading-none mb-0 w-full"
            />
          </div>
        </div>
      }
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={handleCancel}
        >
          Tutup
        </Button>,
        <Button
          loading={loading}
          disabled={uploading}
          onClick={handleSubmit}
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faSave}
            />
          }
          key="open"
          type="primary"
        >
          Simpan
        </Button>,
      ]}
    >
      <TextArea
        name="description"
        className="min-h-[30vh] mt-4 p-4"
        autoSize
        value={form.description}
        onChange={handleChange}
      />
      {
        fileList && <Upload
          onChange={handleUploadChange}          
          listType="picture"
          maxCount={1}          
          defaultFileList={fileList}
          className="w-full mt-4 text-right"
          customRequest={(e) => fbUploader(e)}
        >
          <Button
            icon={
              <FontAwesomeIcon
                className="w-4 h-4 mr-3 inline-block"
                icon={faUpload}
              />
            }
          >
            Unggah dokumen (Max: 1)
          </Button>
        </Upload>
      }
    </Modal>
  );
};

export default SubjectCreate;
