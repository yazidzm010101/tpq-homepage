import React from "react";
import { Header } from "antd/lib/layout/layout";
import { Dropdown, Menu } from "antd";
import { twMerge } from "tailwind-merge";
import Link from "next/link";
import { useRouter } from "next/router";
import Sidebar from "./SidebarMobile";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars, faHamburger } from "@fortawesome/free-solid-svg-icons";

function useScrollingEffect(callback, dependencies) {
  React.useEffect(() => {
    const threshold = document.getElementById("navtop").offsetHeight / 2;
    let lastScrollY = window.pageYOffset;
    let ticking = false;

    const updateScrollDir = () => {
      const scrollY = window.pageYOffset;

      if (Math.abs(scrollY - lastScrollY) < threshold) {
        ticking = false;
        return;
      }

      typeof callback === "function" &&
        callback({ lastScrollY, scrollY, ticking, threshold });
      lastScrollY = scrollY > 0 ? scrollY : 0;
      ticking = false;
    };

    const onScroll = () => {
      if (!ticking) {
        window.requestAnimationFrame(updateScrollDir);
        ticking = true;
      }
    };

    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll);
  }, dependencies);
}

function Navbar({ config }) {
  const [sidebar, setSidebar] = React.useState(false);
  const [scrollDown, setScrollDown] = React.useState(false);
  const [onContent, setOnContent] = React.useState(false);
  const isSolid = onContent;
  const isHidden = onContent && scrollDown;
  const { asPath } = useRouter();
  const logoCN = asPath.startsWith("/about") && !isSolid
    ? "opacity-0 lg:opacity-100"
    : "opacity-100"

  const getClassName = (href, isStrict) => {
    var arrHref = [].concat(href);    
    arrHref = arrHref.filter((itemHref) => {
      var isActive = isStrict
        ? asPath == itemHref
        : asPath.startsWith(itemHref);
      return isActive;
    });
    if (arrHref.length == 0) {
      return "text-gray-700";
    } else {
      return "font-bold";
    }
  };

  const getIndicatorClassName = (href, isStrict) => {
    var arrHref = [].concat(href);
    arrHref = arrHref.filter((itemHref) => {
      var isActive = isStrict
        ? asPath == itemHref
        : asPath.startsWith(itemHref);
      return isActive;
    });
    if (arrHref.length == 0) {
      return "bg-opacity-0";
    } else {
      return "bg-opacity-100";
    }
  };

  useScrollingEffect(
    ({ lastScrollY, scrollY, threshold }) => {
      setScrollDown(scrollY > lastScrollY);
      setOnContent(scrollY > threshold);
    },
    [scrollDown]
  );

  console.log(config.data);

  return (
    <>
      <Header
        id="navtop"
        className={twMerge(
          "navbar fixed bg-white text-gray-800 px-0 z-[2] h-[4.5rem] w-full items-center transition-all",
          isSolid || sidebar ? "bg-opacity-100 shadow-lg" : "bg-opacity-0 shadow-none",
          !isHidden || sidebar ? "top-0" : "-top-[4.5rem]"
        )}
      >
        <div className="relative max-w-7xl px-4 h-full w-full flex mx-auto">
          <div className="flex items-center">          
            <a onClick={() => setSidebar(!sidebar)} className="w-12 text-gray-900 h-12 p-3 mr-4 flex lg:hidden items-center hover:bg-opacity-10 bg-black bg-opacity-0 transition-all rounded-lg">
              <FontAwesomeIcon icon={faBars} className="w-full h-full"/>
            </a>
          </div>
          <Link href="/">
            <a className={twMerge("h-full flex items-center py-2 transition-all", logoCN)}>
              <img
                src={config.logo}
                className="h-10 w-auto inline-block"
                alt="tpq"
              />
              <span className="inline-block ml-2 font-bold text-green text-lg">
                El-Rahmah
              </span>
            </a>
          </Link>
          <div
            mode="horizontal"
            className="hidden lg:flex uppercase items-center children:mx-1 absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 ml-4 h-full py-2 bg-transparent border-0"
          >
            {config.data.map((item, index) => {
              if (Array.isArray(item.submenu)) {
                return (
                  <Dropdown
                    key={index}
                    overlay={
                      <Menu
                        className="rounded-lg"
                        items={item.submenu.map((submenu, indexChild) => {
                          return {
                            key: "" + index + indexChild,
                            label: (
                              <Link href={submenu.href}>
                                <a className="h-full w-full inline-block">
                                  {submenu.label}
                                </a>
                              </Link>
                            ),
                          };
                        })}
                      />
                    }
                  >
                    <div
                      className="relative flex items-center bg-transparent hover:bg-black hover:bg-opacity-10 rounded-lg transition-all px-4"
                      key={index}
                    >
                      <a
                        className={twMerge(
                          "relative h-full w-full inline-block",
                          getClassName(
                            item.submenu.map((item) => item.href),
                            item.isStrict
                          )
                        )}
                        onClick={(e) => e.preventDefault()}
                      >
                        {item.label}
                      </a>
                      <div
                        className={twMerge(
                          "bg-[#1b5e41] w-[50%] max-w-[64px] h-[4px] rounded-sm absolute bottom-0 left-1/2 -translate-x-1/2",
                          getIndicatorClassName(
                            item.submenu.map((item) => item.href),
                            item.isStrict
                          )
                        )}
                      ></div>
                    </div>
                  </Dropdown>
                );
              }
              return (
                <div
                  className="relative flex items-center bg-transparent hover:bg-black hover:bg-opacity-10 rounded-lg transition-all px-4"
                  key={index}
                >
                  <Link href={item.href}>
                    <a
                      className={twMerge(
                        "relative h-full w-full whitespace-nowrap inline-block",
                        getClassName(item.href, item.isStrict)
                      )}
                    >
                      {item.label}
                    </a>
                  </Link>
                  <div
                    className={twMerge(
                      "bg-[#1b5e41] w-[50%] max-w-[64px] h-[4px] rounded-sm absolute bottom-0 left-1/2 -translate-x-1/2",
                      getIndicatorClassName(item.href, item.isStrict)
                    )}
                  ></div>
                </div>
              );
            })}
          </div>
        </div>
      </Header>
      <Sidebar config={config} active={sidebar} className={"lg:hidden"}/>
    </>
  );
}

export default Navbar;
