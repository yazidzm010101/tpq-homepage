import {
  Avatar,
  Button,  
  Input,
  List,
  Modal,
  notification,
  Skeleton,
  Typography,
} from "antd";

import {
  faExclamationCircle,
  faX,
  faRotate,
  faSave,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import TextArea from "antd/lib/input/TextArea";
import { useDocument } from "swr-firestore-v9";
import { dateToString } from "../../utils/utils";

const NewsDelete = ({ id, visible = false, onCancel }) => {
  const { deleteDocument } = useDocument("news/" + id);
  const [ loading, setLoading ] = React.useState(false)
  const handleSubmit = async () => {
    try {
      setLoading(true)
      await deleteDocument()
      setLoading(false)      
      notification.info({
        message: 'Sukses',
        description: 'Data berhasil dihapus!',
        placement: 'bottomRight'
      })
      onCancel()
    } catch (err) {
      setLoading(false)      
      notification.error({
        message: 'Peringatan',
        description: 'Gagal menghapus data, silahkan coba beberapa saat lagi!',
        placement: 'bottomRight'
      })
    }

  }
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={"Hapus data"}
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={onCancel}
        >
          Tutup
        </Button>,
        <Button
          loading={loading}
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faTrash}
            />
          }
          key="close"
          onClick={handleSubmit}
          type="danger"
        >
          Hapus
        </Button>,
      ]}
    >
      Anda yakin ingin menghapus data ini?
    </Modal>
  );
};

const NewsModal = ({
  visible = false,
  id,
  title,
  date,  
  description,
  onChange,
  onCancel,
}) => {
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false)
  const { update } = useDocument("news/" + id);
  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
    onChange({ [e.target.name]: e.target.value });
  };
  const handleSubmit = async () => {
    try {
      setLoading(true)
      await update(form)
      setLoading(false)      
      notification.info({
        message: 'Sukses',
        description: 'Data berhasil disimpan!',
        placement: 'bottomRight'
      })
      onCancel()
    } catch (err) {
      setLoading(false)      
      notification.error({
        message: 'Peringatan',
        description: 'Gagal menyimpan perubahan, silahkan coba beberapa saat lagi!',
        placement: 'bottomRight'
      })
    }

  }
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={
        <div className="flex">
          <Avatar
            shape="square"
            size={"large"}
            className="bg-[#e65245] text-3xl flex items-center m-0 leading-none"
          >
            {!!title && title[0]}
          </Avatar>
          <div className="ml-4">
            <Input
              type="text"
              className="text-xl leading-none mb-3"
              value={title}
              name="title"
              placeholder="Judul"
              onChange={handleChange}
            />
            <br />
            <Input
              type="date"
              name="date"
              className="text-gray-500 w-max text-xs"
              value={date}
              onChange={handleChange}
            />            
          </div>
        </div>
      }
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={onCancel}
        >
          Tutup
        </Button>,
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faSave}
            />
          }
          loading={loading}
          key="close"
          onClick={handleSubmit}
          type="primary"          
        >
          Simpan
        </Button>,
      ]}
    >
      <TextArea
        className="p-4"
        placeholder="Deskripsi"
        autoSize
        name="description"        
        value={description}
        onChange={handleChange}
      />
    </Modal>
  );
};

const NewsManager = ({ data, pageSize = 12, loading, error }) => {
  const [detail, setDetail] = React.useState();
  const [deleteData, setDeleteData] = React.useState();
  if (!!loading) {
    data = Array.from(Array(10).keys());
    return (
      <List
        className="rounded-xl bg-white"
        dataSource={data}
        renderItem={() => (
          <List.Item className="p-6">
            <Skeleton active />
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          className="text-primary-custom h-16"
        />
        <h5 className="mb-0 text-primary-custom">
          Belum ada data terbaru saat ini
        </h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        className="rounded-xl bg-white py-3"
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item
            className="px-6"
            actions={[
              <a onClick={() => setDetail(item)} key="list-loadmore-edit">
                ubah
              </a>,
              <a
                onClick={() => setDeleteData(item)}
                className="text-red-600"
                key="list-loadmore-delete"
              >
                hapus
              </a>,
            ]}
          >
            <List.Item.Meta
              title={item.title}
              description={
                <>
                  <Typography.Paragraph ellipsis={{ rows: 2 }}>
                    {item.description}
                  </Typography.Paragraph>
                  <Typography.Paragraph className="text-gray-400">
                    { dateToString(item.date) }
                  </Typography.Paragraph>
                </>
              }
            />
          </List.Item>
        )}
      />
      <NewsModal
        visible={!!detail}
        id={!!detail && detail.id}
        title={!!detail && detail.title}
        date={!!detail && detail.date}        
        description={!!detail && detail.description}
        onChange={(newDetail) => setDetail({ ...detail, ...newDetail })}
        onCancel={() => setDetail(null)}
      />
      <NewsDelete
        visible={!!deleteData}
        id={!!deleteData && deleteData.id}        
        onCancel={() => setDeleteData(null)}
      />
    </>
  );
};

export default NewsManager;
