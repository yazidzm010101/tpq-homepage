import { faSave, faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Avatar, Button, Input, Modal, notification, Typography } from "antd";
import TextArea from "antd/lib/input/TextArea";
import React from "react";

export default function NewsCreate({
  visible = false,    
  onSubmit,
  onCancel,
}) {
  const [form, setForm] = React.useState({});  
  const [ loading, setLoading ] = React.useState(false)
  const handleChange = (e) => {    
    setForm({ ...form, [e.target.name]: e.target.value });    
    console.log(form)
  };
  const handleCancel = () => {
    setForm({})
    onCancel()
  }
  const handleSubmit = async () => {
    try {
      setLoading(true)
      await onSubmit(form)
      setLoading(false)
      notification.info({
        message: 'Sukses',
        description: 'Data berhasil ditambah!',
        placement: 'bottomRight'
      })
      setForm({})
      onCancel()
    } catch (err) {
      notification.error({
        message:'Peringatan',
        description: 'Gagal menambahkan data, silahkan coba beberapa saat lagi!',
        placement: 'bottomRight'
      })
    }
  }
  React.useEffect(() => {
    if (!visible) {
      setForm({})
    }
  }, [visible])
  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      title={"Tambah Data"}
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={handleCancel}
        >
          Tutup
        </Button>,
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faSave}
            />
          }
          key="save"
          loading={loading}
          onClick={handleSubmit}
          type="primary"
        >
          Tambah
        </Button>,
      ]}
    >      
            <Input
              type="text"
              className="text-xl leading-none mb-3"              
              name="title"
              placeholder="Judul"
              value={form.title}
              onChange={handleChange}
            />
            <br />
            <Input
              type="date"
              name="date"
              size="large"
              value={form.date}
              className="text-gray-500 w-max mb-3 text-xs"              
              onChange={handleChange}
            />                  
      <TextArea
        className="p-4 mb-3"
        placeholder="Deskripsi"
        autoSize
        name="description"
        value={form.description}
        onChange={handleChange}
      />
    </Modal>
  );
};
