import { Avatar, Button, Card, List, Modal, Skeleton, Typography } from "antd";
import { dateToString } from '../../utils/utils'

import {
  faExclamationCircle,
  faX,
  faRotate,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";

const NewsItem = ({ date, title, body, onClick }) => {
  return (
    <Card
      size="small"
      className="border-[rgba(0,0,0,0.025)] hover:bg-gray-100 transition-all duration-300"
      style={{ boxShadow: "inset 0 -1px 0 0 rgba(0, 0, 0, 0.05)" }}
      hoverable
      onClick={onClick}      
      draggable
    >
      <Typography.Title ellipsis={{ rows: 2 }} level={5}>        
        {title}
      </Typography.Title>
      <Typography.Paragraph className="tx-xs text-gray-400">
        {dateToString(date)}
      </Typography.Paragraph>
      <Typography.Paragraph ellipsis={{ rows: 3 }}>{body}</Typography.Paragraph>
    </Card>
  );
};

const NewsModal = ({
  visible = false,
  date = "",
  title = "",
  body = "",
  onCancel,
}) => {
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={
        <div className="flex">
          <Avatar
            shape="square"
            size={"large"}
            className="bg-[#e65245] text-3xl flex items-center m-0 leading-none"
          >
            {!!title && title[0]}
          </Avatar>
          <div className="ml-4">
            <Typography.Title level={3} className="leading-none mb-0">
              {title}
            </Typography.Title>
            <Typography.Text className="text-gray-500 text-xs">
              {date}
            </Typography.Text>
          </div>
        </div>
      }
      footer={null}
    >
      <Typography.Paragraph className="min-h-[30vh]">
        {body}
      </Typography.Paragraph>
    </Modal>
  );
};

const News = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 2,
  xl = 3,
  xxl = 3,
  pageSize = 12,
  loading,
  error,
 skeletonLimit = 10
}) => {
  const [detail, setDetail] = React.useState();
  if (!!loading) {
    data = Array.from(Array(skeletonLimit).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}          
          className="text-primary-custom h-16"
        />
        <h5 className="mb-0 text-primary-custom">
          Belum ada data terbaru saat ini
        </h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item>
            <NewsItem
              onClick={() => setDetail(item)}
              date={item.date}
              title={item.title}
              body={item.description}
            />
          </List.Item>
        )}
      />
      <NewsModal
        visible={!!detail}
        date={detail && detail.date}
        detail
        title={detail && detail.title}
        body={detail && detail.description}
        onCancel={() => setDetail(null)}
      />
    </>
  );
};

export default News;
