import React from "react";
import { Menu } from "antd";
import { twMerge } from "tailwind-merge";
import Link from "next/link";
import Sider from "antd/lib/layout/Sider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function SidebarMobile({ config, active, className: overrideClassName }) {
  const className = active 
    ? "top-0 left-0"
    : "top-0 -left-[300px]"

  return (
    <Sider className={twMerge("z-[1] shadow-xl fixed min-h-[100vh] w-[300px] max-w-full pt-[4.5rem] bg-white", className, overrideClassName)}>
      <Menu
        mode="vertical"
        className="h-full w-full"
      >
        {config.data.map((item, index) => {
          if (Array.isArray(item.submenu)) {
            return (
              <Menu.SubMenu
                key={index}
                title={item.label}
                icon={
                  <FontAwesomeIcon
                    className="h-[1rem] w-auto inline-block"
                    icon={item.faIcon}
                  />
                }
              >
                {item.submenu.map((submenu, index) => (
                  <Menu.Item
                    key={index}
                    icon={
                      <FontAwesomeIcon
                        className="h-[1rem] w-auto inline-block"
                        icon={submenu.faIcon}
                      />
                    }
                  >
                    <Link href={submenu.href}>
                      <a className="h-full w-full inline-block">
                        {submenu.label}
                      </a>
                    </Link>
                  </Menu.Item>
                ))}
              </Menu.SubMenu>
            );
          }
          return (
            <Menu.Item
              className="flex items-center"
              key={index}
              icon={
                <FontAwesomeIcon
                  className="h-[1rem] w-auto inline-block"
                  icon={item.faIcon}
                />
              }
            >
              <Link href={item.href}>
                <a className="h-full w-full inline-block">{item.label}</a>
              </Link>
            </Menu.Item>
          );
        })}
      </Menu>
    </Sider>
  );
}

export default SidebarMobile;
