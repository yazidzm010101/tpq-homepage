import {
  Avatar,
  Button,
  Card,
  Input,
  List,
  Modal,
  notification,
  Skeleton,
  Typography,
  Upload,
} from "antd";

import {
  faExclamationCircle,
  faX,
  faRotate,
  faFileImage,
  faCircleNotch,
  faSave,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import firebaseUploader from "../../utils/firebaseUpload";
import { useDocument } from "swr-firestore-v9";

const GalleryDelete = ({ id, visible = false, onCancel, onDeleted }) => {
  const { deleteDocument } = useDocument("gallery/" + id);
  const [ loading, setLoading ] = React.useState(false)
  const handleSubmit = async () => {
    try {
      setLoading(true)
      await deleteDocument()
      setLoading(false)      
      notification.info({
        message: 'Sukses',
        description: 'Data berhasil dihapus!',
        placement: 'bottomRight'
      })
      onCancel()
      onDeleted()
    } catch (err) {
      setLoading(false)      
      notification.error({
        message: 'Peringatan',
        description: 'Gagal menghapus data, silahkan coba beberapa saat lagi!',
        placement: 'bottomRight'
      })
    }

  }
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={"Hapus data"}
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={onCancel}
        >
          Tutup
        </Button>,
        <Button
          loading={loading}
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faTrash}
            />
          }
          key="close"
          onClick={handleSubmit}
          type="danger"
        >
          Hapus
        </Button>,
      ]}
    >
      Anda yakin ingin menghapus data ini?
    </Modal>
  );
};

function GalleryUpdate({
  id,
  filePath,
  name,
  visible = false,  
  onChange,
  onDelete,
  onCancel,
}) {
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [uploading, setUploading] = React.useState(false);  
  const { update } = useDocument("gallery/" + id);
  const fbUploader = firebaseUploader("gallery");
  const [ deleteShown, setDeleteShown ] = React.useState(false);

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
      createdAt: new Date().getTime(),
    });
    onChange({ [e.target.name]: e.target.value });
  };  
  const handleUploadChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
    }
    if (info.file.status === "done") {
      setForm({ ...form, filePath: info.file.response });
      onChange({ filePath: info.file.response });
      setUploading(false);
      console.log(form);
    }
    console.log(info);
  };
  const handleCancel = () => {
    setForm({});
    onCancel();
  };  
  const handleSubmit = async () => {
    try {
      setLoading(true);
      await update(form);
      setLoading(false);
      notification.info({
        message: "Sukses",
        description: "Data berhasil disimpan!",
        placement: "bottomRight",
      });
      onCancel();
    } catch (err) {
      setLoading(false);
      notification.error({
        message: "Peringatan",
        description:
          "Gagal menyimpan perubahan, silahkan coba beberapa saat lagi!",
        placement: "bottomRight",
      });
    }
  };
  React.useEffect(() => {
    if (!visible) {
      setForm({});
    }
  }, [visible]);

  const uploadButton = (
    <div className="text-gray-800 flex flex-col items-center justify-center w-full h-full">
      {uploading ? (
        <FontAwesomeIcon icon={faCircleNotch} className="w-8 h-auto fa-spin" />
      ) : (
        <FontAwesomeIcon icon={faFileImage} className="w-8 h-auto" />
      )}
      Unggah
    </div>
  );

  return (
      <>
      <Modal
        visible={visible}
        onCancel={handleCancel}
        title={"Detail Data"}
        footer={[
          <Button
            icon={
              <FontAwesomeIcon
                className="h-3 mb-0.5 mr-1 inline-block"
                icon={faTrash}
              />
            }
            key="trash"
            type="secondary"
            className="text-red-500 hover:border-red-500"
            onClick={() => setDeleteShown(true)}
          >
            Hapus
          </Button>,
          <Button
            disabled={uploading}
            loading={loading}
            icon={
              <FontAwesomeIcon
                className="h-3 mb-0.5 mr-1 inline-block"
                icon={faSave}
              />
            }
            key="save"
            onClick={handleSubmit}
            type="primary"
          >
            Simpan
          </Button>,
        ]}
      >
        <div className="flex">
          <Upload
            showUploadList={false}
            onChange={handleUploadChange}
            className="avatar-uploader inline-block w-max"
            listType="picture-card"
            customRequest={(e) => fbUploader(e)}
          >
            {filePath && !uploading ? (
              <img src={filePath} alt="avatar" style={{ width: "100%" }} />
            ) : (
              uploadButton
            )}
          </Upload>
          <Input
            type="text"
            className="text-xl w-full h-max leading-none mb-3"
            name="name"
            placeholder="Nama"
            value={name}
            onChange={handleChange}
          />
        </div>
      </Modal>
      <GalleryDelete id={id} visible={deleteShown} onCancel={() => setDeleteShown(false)} onDeleted={() => onCancel()}/>
    </>
  );
}

const GalleryItem = ({ filePath, name, onClick }) => {
  return (
    <div className="hover:cursor-pointer">
      <div className="pt-[60%] bg-gray-200 rounded-xl h-0 bg-cover bg-center" style={{ background: 'url("' + filePath + '")' }}>        
      </div>
      <Typography.Paragraph className="m-1" ellipsis={{ rows: 3 }}>{name}</Typography.Paragraph>
    </div>
  );
};

const Gallery = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 3,
  xl = 4,
  xxl = 4,
  pageSize = 12,
  loading,
  error,
}) => {
  const [detail, setDetail] = React.useState();
  if (!!loading) {
    data = Array.from(Array(8).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <h5 className="mb-0 text-primary-custom">Galeri masih kosong</h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item onClick={() => setDetail(item)}>
            <GalleryItem filePath={item.filePath} name={item.name} />
          </List.Item>
        )}
      />
      <GalleryUpdate
        visible={!!detail}
        id={!!detail && detail.id}
        name={!!detail && detail.name}
        filePath={!!detail && detail.filePath}
        onChange={(newDetail) => setDetail({ ...detail, ...newDetail })}
        onCancel={() => setDetail(null)}
      />
    </>
  );
};

export default Gallery;
