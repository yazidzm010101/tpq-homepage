import {
  Avatar,
  Button,
  Card,
  Image,
  List,
  Modal,
  Skeleton,
  Typography,
} from "antd";

import {
  faExclamationCircle,
  faX,
  faRotate,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";

const GalleryItem = ({
  filePath,
  name,  
}) => {
  return (
    <div>
      <Image src={filePath} alt="Alt" />
      <Typography.Paragraph ellipsis={{ rows: 3 }}>
        {name}
      </Typography.Paragraph>
    </div>
  );
};

const Gallery = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 3,
  xl = 4,
  xxl = 4,
  pageSize = 12,
  loading,
  error,
}) => {  
  if (!!loading) {
    data = Array.from(Array(8).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <h5 className="mb-0 text-primary-custom">Galeri masih kosong</h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item>
            <GalleryItem name={item.name} filePath={item.filePath} />
          </List.Item>
        )}
      />
    </>
  );
};

export default Gallery;
