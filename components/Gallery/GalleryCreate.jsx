import {
    faAdd,
  faCircleNotch,
  faFileImage,
  faFileUpload,
  faSave,
  faSpinner,
  faUpload,
  faX,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Avatar,
  Button,
  Input,
  Modal,
  notification,
  Typography,
  Upload,
} from "antd";
import TextArea from "antd/lib/input/TextArea";
import React from "react";
import firebaseUploader from "../../utils/firebaseUpload";

export default function GalleryCreate({ visible = false, onSubmit, onCancel }) {
  const [form, setForm] = React.useState({});
  const [loading, setLoading] = React.useState(false);
  const [uploading, setUploading] = React.useState(false);
  const [imageUrl, setImageUrl] = React.useState();
  const fbUploader = firebaseUploader("gallery");

  const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener("load", () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
      createdAt: new Date().getTime(),
    });
    console.log(form);
  };
  const handleUploadChange = (info) => {
    if (info.file.status === "uploading") {
      setUploading(true);
    }
    if (info.file.status === "done") {
      setForm({ ...form, filePath: info.file.response });
      setUploading(false);
      console.log(form);
      getBase64(info.file.originFileObj, (url) => {
        setImageUrl(url);
      });
    }
    console.log(info);
  };
  const handleCancel = () => {
    setForm({});
    onCancel();
  };
  const handleSubmit = async () => {
    try {
      setLoading(true);
      await onSubmit(form);
      setLoading(false);
      notification.info({
        message: "Sukses",
        description: "Data berhasil ditambah!",
        placement: "bottomRight",
      });
      setForm({});
      onCancel();
    } catch (err) {
      notification.error({
        message: "Peringatan",
        description:
          "Gagal menambahkan data, silahkan coba beberapa saat lagi!",
        placement: "bottomRight",
      });
    }
  };
  React.useEffect(() => {
    if (!visible) {
      setForm({});
    }
  }, [visible]);

  const uploadButton = (
    <div className="text-gray-800 flex flex-col items-center justify-center w-full h-full">
      {uploading ? (
        <FontAwesomeIcon icon={faCircleNotch} className="w-8 h-auto fa-spin"/>
      ) : (
        <FontAwesomeIcon icon={faFileImage} className="w-8 h-auto" />
      )}    
        Unggah      
    </div>
  );

  return (
    <Modal
      visible={visible}
      onCancel={handleCancel}
      title={"Tambah Data"}
      footer={[
        <Button
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faX}
            />
          }
          key="close"
          type="secondary"
          onClick={handleCancel}
        >
          Tutup
        </Button>,
        <Button
          disabled={uploading}
          loading={loading}
          icon={
            <FontAwesomeIcon
              className="h-3 mb-0.5 mr-1 inline-block"
              icon={faSave}
            />
          }
          key="save"
          onClick={handleSubmit}
          type="primary"
        >
          Tambah
        </Button>,
      ]}
    >
        <div className="flex">
            <Upload
                showUploadList={false}
                onChange={handleUploadChange}
                className="avatar-uploader inline-block w-max"
                listType="picture-card"
                customRequest={(e) => fbUploader(e)}
            >
                {imageUrl && !uploading ? (
                <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
                ) : (
                uploadButton
                )}
            </Upload>
            <Input
                type="text"
                className="text-xl w-full h-max leading-none mb-3"
                name="name"
                placeholder="Nama"
                value={form.name}
                onChange={handleChange}
            />
        </div>
    </Modal>
  );
}
