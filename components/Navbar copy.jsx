import React from "react";
import { Header } from "antd/lib/layout/layout";
import { Menu } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { twMerge } from "tailwind-merge";
import Link from "next/link";

function useScrollingEffect(callback, dependencies) {
  React.useEffect(() => {
    const threshold = document.getElementById("navtop").offsetHeight;
    let lastScrollY = window.pageYOffset;
    let ticking = false;

    const updateScrollDir = () => {
      const scrollY = window.pageYOffset;

      if (Math.abs(scrollY - lastScrollY) < threshold) {
        ticking = false;
        return;
      }

      typeof callback === "function" &&
        callback({ lastScrollY, scrollY, ticking, threshold });
      lastScrollY = scrollY > 0 ? scrollY : 0;
      ticking = false;
    };

    const onScroll = () => {
      if (!ticking) {
        window.requestAnimationFrame(updateScrollDir);
        ticking = true;
      }
    };

    window.addEventListener("scroll", onScroll);

    return () => window.removeEventListener("scroll", onScroll);
  }, dependencies);
}

function Navbar({ config }) {
  const [scrollDown, setScrollDown] = React.useState(false);
  const [onContent, setOnContent] = React.useState(false);
  const isSolid = onContent;
  const isHidden = onContent && scrollDown;

  useScrollingEffect(
    ({ lastScrollY, scrollY, threshold }) => {
      setScrollDown(scrollY > lastScrollY);
      setOnContent(scrollY > threshold);
    },
    [scrollDown]
  );

  return (
    <Header
      id="navtop"
      mode="horizontal"
      className={twMerge(
        "navbar fixed bg-white text-gray-800 px-12 flex z-[2] h-[4.5rem] w-full items-center transition-all",
        isSolid ? "bg-opacity-100 shadow-md" : "bg-opacity-0 shadow-none",
        !isHidden ? "top-0" : "-top-[4.5rem]"
      )}
    >
      <Link href="/">
        <a className="h-full flex items-center py-2">
          <img
            src={config.logo}
            className="h-full w-auto inline-block"
            alt="tpq"
          />
          <span className="inline-block ml-2 font-bold text-green text-lg">
            El-Rahman
          </span>
        </a>
      </Link>
      <Menu
        mode="horizontal"
        className="mx-auto h-full py-2 bg-transparent border-0"
      >
        {config.data.map((item, index) => {
          if (Array.isArray(item.submenu)) {
            return (
              <Menu.SubMenu
                key={index}
                title={item.label}
                icon={
                  <FontAwesomeIcon
                    className="h-[1rem] w-auto inline-block"
                    icon={item.faIcon}
                  />
                }
              >
                {item.submenu.map((submenu, index) => (
                  <Menu.Item
                    key={index}
                    icon={
                      <FontAwesomeIcon
                        className="h-[1rem] w-auto inline-block"
                        icon={submenu.faIcon}
                      />
                    }
                  >
                    <Link href={submenu.href}>
                      <a className="h-full w-full inline-block">
                        {submenu.label}
                      </a>
                    </Link>
                  </Menu.Item>
                ))}
              </Menu.SubMenu>
            );
          }
          return (
            <Menu.Item
              className="flex items-center"
              key={index}
              icon={
                <FontAwesomeIcon
                  className="h-[1rem] w-auto inline-block"
                  icon={item.faIcon}
                />
              }
            >
              <Link href={item.href}>
                <a className="h-full w-full inline-block">{item.label}</a>
              </Link>
            </Menu.Item>
          );
        })}
      </Menu>
    </Header>
  );
}

export default Navbar;
