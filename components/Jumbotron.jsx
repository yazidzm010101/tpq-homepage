import React from "react";
import { Button, Carousel, Typography } from "antd";
import Image from "next/image";
import News from "../components/News/News";
import { useCollection } from "swr-firestore-v9";
import Link from "next/link";

function Jumbotron() {
  const news = useCollection("news", {
    orderBy: ["date", "desc"],
    limit: 3
  });
  return (
    <Carousel>
      <div>
        <div
          className="min-h-[100vh] py-30 pt-20 px-0 bg-gray-50 bg-cover bg-center flex flex-col items-center justify-center"
          style={{ background: 'url("background.jpg")' }}
        >
            <div className="w-full h-full absolute top-0 left-0 bg-gray-50 opacity-70"></div>
            <div className="relative pt-20 pb-28 h-full w-full text-center">
                <div className="w-[12rem] h-[12rem] mx-auto relative">
                  <Image src="/tpq.png" alt="tpq" layout="fill" objectFit="contain"/>
                </div>
                <Typography.Paragraph className="text-center my-0 mt-3 leading-none">
                    Taman Pendidikan Al-Quran
                </Typography.Paragraph>
                <Typography.Title level={2} className="leading-relaxed text-center my-0">
                    El-Rahmah
                </Typography.Title>
                <Typography.Title level={4} className="leading-relaxed text-center my-0 font-normal max-w-md mx-auto">
                  Menjadikan Santri yang Saleh dan Berakhlakul Karimah (SARAH)
                </Typography.Title>                
            </div>
            {
              news.data && (
                <div className="relative mb-20 max-w-4xl mx-auto">
                  <Typography.Title level={4} className="leading-relaxed text-center font-normal max-w-md mx-auto">
                    Berita terakhir
                  </Typography.Title>
                  <News data={news.data} className="w-fulll" loading={news.loading} error={news.error} skeletonLimit={3}/>
                  <div className="text-center">
                    <Link href="/news">
                      <Button type="primary" size="large" href="/news" className="inline-block max-w-[max-content]">Lihat Semua</Button>
                    </Link>
                  </div>
                </div>
              )
            }
        </div>        
      </div>
    </Carousel>
  );
}

export default Jumbotron;
