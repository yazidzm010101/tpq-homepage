import { Avatar, Button, Card, List, Modal, Skeleton, Typography } from "antd";

import {
  faExclamationCircle,
  faX,
  faRotate,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { dateToString } from "../../utils/utils"

import React from "react";

const ActivityItem = ({ startDate, endDate, title, body, onClick }) => {
  return (
    <Card
      size="small"
      className="border-[rgba(0,0,0,0.025)] hover:bg-gray-100 transition-all duration-300"
      style={{ boxShadow: "inset 0 -1px 0 0 rgba(0, 0, 0, 0.05)" }}
      hoverable
      onClick={onClick}
      draggable
    >
      <Typography.Title ellipsis={{ rows: 2 }} level={5}>
        {title}
      </Typography.Title>
      <Typography.Paragraph className="tx-xs text-gray-400">
        {dateToString(startDate)} - {dateToString(endDate)}
      </Typography.Paragraph>
      <Typography.Paragraph ellipsis={{ rows: 3 }}>{body}</Typography.Paragraph>
    </Card>
  );
};

const ActivityModal = ({
  visible = false,
  startDate = "",
  endDate = "",
  title = "",
  body = "",
  onCancel,
}) => {
  return (
    <Modal
      visible={visible}
      onCancel={onCancel}
      title={
        <div className="flex">
          <Avatar
            shape="square"
            size={"large"}
            className="bg-[#e65245] text-3xl flex items-center m-0 leading-none"
          >
            {!!title && title[0]}
          </Avatar>
          <div className="ml-4">
            <Typography.Title level={3} className="leading-none mb-0">
              {title}
            </Typography.Title>
            <Typography.Text className="text-gray-500 text-xs">
            {dateToString(startDate)} - {dateToString(endDate)}
            </Typography.Text>
          </div>
        </div>
      }
      footer={null}
    >
      <Typography.Paragraph className="min-h-[30vh]">
        {body}
      </Typography.Paragraph>
    </Modal>
  );
};

const Activities = ({
  data,
  xs = 1,
  sm = 2,
  md = 2,
  lg = 2,
  xl = 3,
  xxl = 3,
  pageSize = 12,
  loading,
  error,
 skeletonLimit = 10
}) => {
  const [detail, setDetail] = React.useState();
  if (!!loading) {
    data = Array.from(Array(10).keys());
    return (
      <List
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={() => (
          <List.Item>
            <Card>
              <Skeleton active />
            </Card>
          </List.Item>
        )}
      />
    );
  }
  if (!!error) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          style={{ fontSize: "4em" }}
          className="text-primary-custom"
        />
        <p className="fw-normal mt-2">Gagal memperoleh data terkini</p>
        <Button type="primary" icon={<FontAwesomeIcon icon={faRotate} />}>
          Muat ulang
        </Button>
      </div>
    );
  }
  if (data.length == 0) {
    return (
      <div className="p-3 text-center text-gray-500 flex flex-col h-full justify-center items-center">
        <FontAwesomeIcon
          icon={faExclamationCircle}
          className="text-primary-custom h-16"
        />
        <h5 className="mb-0 text-primary-custom">
          Belum ada data terbaru saat ini
        </h5>
        <p className="fw-normal mt-2">Silahkan tunggu beberapa saat lagi</p>
      </div>
    );
  }
  return (
    <>
      <List
        pagination={{
          pageSize: pageSize,
          responsive: true,
          hideOnSinglePage: true,
        }}
        grid={{ gutter: [16, 16], xs, sm, md, lg, xl, xxl }}
        dataSource={data}
        renderItem={(item) => (
          <List.Item>
            <ActivityItem
              onClick={() => setDetail(item)}
              startDate={item.startDate}
              endDate={item.endDate}
              title={item.name}
              body={item.description}
            />
          </List.Item>
        )}
      />
      <ActivityModal
        visible={!!detail}
        startDate={detail && detail.startDate}
        endDate={detail && detail.endDate}
        detail
        title={detail && detail.name}
        body={detail && detail.description}
        onCancel={() => setDetail(null)}
      />
    </>
  );
};

export default Activities;
