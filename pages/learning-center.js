import { useCollection } from "swr-firestore-v9";
import HomeLayout from "../layouts/HomeLayout";
import Subjects from "../components/Subjects/Subjects";
import { Typography } from "antd";
import Image from "next/image";

export default function LearningCenter() {
  const learningCenter = useCollection("learning_center", {
    orderBy: ["createdAt", "desc"],
  });  
  return (
    <HomeLayout title={"TPQ"}>
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/book.png" />
          </div>
          E-Learning
        </Typography.Title>
        <Subjects data={learningCenter.data} loading={learningCenter.loading} error={learningCenter.error} />
      </div>
    </HomeLayout>
  );
}
