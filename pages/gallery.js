import HomeLayout from "../layouts/HomeLayout";
import { Typography } from "antd";
import Gallery from "../components/Gallery/Gallery";
import Image from "next/image";
import { useCollection } from "swr-firestore-v9";

export default function Home() {  
  const gallery = useCollection('gallery')
  return (
    <HomeLayout title={"TPQ"}>         
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image              
              layout="fill"
              objectFit="contain"
              src="/gallery.png"              
            />
          </div>
          Galeri
        </Typography.Title>
        <Gallery data={gallery.data} loading={gallery.loading} error={gallery.error} />        
      </div>
    </HomeLayout>
  );
}
