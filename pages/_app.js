require('antd/lib/style/themes/default.less');
require('antd/dist/antd.less');
import '../styles/globals.css'
import 'firebase/firestore';
import 'firebase/auth';
import { useCookies } from 'react-cookie';
import { Fuego, FuegoProvider } from 'swr-firestore-v9';
import firebaseConfig from '../configs/firebaseConfig';
import React from 'react';
import { useRouter } from 'next/router';
import jwt from 'jsonwebtoken';
import { redirect } from 'next/dist/server/api-utils';
import Router from 'next/router'

const fuego = new Fuego(firebaseConfig);

function App({ Component, pageProps }) {
  const [cookies, setCookies] = useCookies(['key']);
  const { pathname } = useRouter();
  React.useEffect(() => {        
    console.log(pathname)
    jwt.verify(cookies.key, 'qnfudi13p2p  sx', (err, decoded) => {
      console.log(decoded)
      if (decoded && decoded.username == 'tpq-elrahman-admin') {
        if (pathname == '/dashboard') {
          Router.push('/dashboard/activities')
        }
      } else if (pathname.startsWith('/dashboard')) {
        Router.push('/dashboard')
      }
    })
  }, [pathname, cookies.key])
  return (
    <FuegoProvider fuego={fuego}>
      <Component {...pageProps} />
    </FuegoProvider>
  )
}

export default App;
