import { useCollection } from "swr-firestore-v9";
import HomeLayout from "../layouts/HomeLayout";
import Jumbotron from "../components/Jumbotron";

export default function Home() {
  const activities = useCollection("activity");
  const gallery = useCollection("gallery");
  console.log(activities.data);
  console.log(gallery.data);
  return (
    <HomeLayout title={"TPQ"}>
      <Jumbotron />      
    </HomeLayout>
  );
}
