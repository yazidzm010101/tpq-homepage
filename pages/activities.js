import { useCollection } from "swr-firestore-v9";
import HomeLayout from "../layouts/HomeLayout";
import { Button, Typography } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import Activities from "../components/Activity/Activities"
import Image from "next/image";

export default function News() {
  const news = useCollection("activity", {
    orderBy: ["endDate", "desc"],
  });  
  return (
    <HomeLayout title={"TPQ"}>
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">        
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/event.png" />
          </div>
          Kegiatan
        </Typography.Title>        
        <Activities
          data={news.data}
          loading={news.loading}
          error={news.error}
        />
      </div>
    </HomeLayout>
  );
}
