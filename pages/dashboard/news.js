import { useCollection } from "swr-firestore-v9";
import { Button, Typography } from "antd";
import Image from "next/image";
import DashboardLayout from "../../layouts/DashboardLayout";
import React from "react";
import NewsCreate from "../../components/News/NewsCreate";
import NewsManager from "../../components/News/NewsManager";

export default function News() {
  const news = useCollection("news", {
    orderBy: ["date", "desc"],
  });
  const [ createShown, setCreateShown ] = React.useState(false)
  return (
    <DashboardLayout title={"TPQ"}>
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">        
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/news.png" />
          </div>
          Berita
        </Typography.Title>                
        <NewsManager
          data={news.data}
          loading={news.loading}
          error={news.error}
        />
        <NewsCreate
          visible={createShown}
          onCancel={() => setCreateShown(false)}
          onSubmit={async (data) => {
            await news.add(data)
            setCreateShown(false)
          }}
        />
        <Button type="primary" size="large" className="mb-3 fixed bottom-10 right-10 shadow-lg"
          onClick={() => setCreateShown(true)}>+ Tambah</Button>
      </div>
    </DashboardLayout>
  );
}
