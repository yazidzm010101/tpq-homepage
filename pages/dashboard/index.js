import { useCollection } from "swr-firestore-v9";
import { Button, Form, Input, notification, Typography } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";
import Head from "next/head";
import jwt from 'jsonwebtoken';
import { useCookies } from "react-cookie";
import Router from "next/router";

export default function News() {   
  const [cookies, setCookies] = useCookies(['key']);
  const news = useCollection("activity", {
    orderBy: ["endDate", "desc"],
  });

  const onFinish = (values) => {    
    if (values.username === 'tpq-elrahman-admin'&& values.password === '#TPQ*EL_rahmah99') {            
      var key = jwt.sign({ username: values.username}, 'qnfudi13p2p  sx', { expiresIn: 24 * 3600 })
      var expires = new Date(Date.now() + (24 * 3600 * 1000))            
      setCookies('key', key, { path: '/', expires });      
      Router.push('/dashboard/activities')
      notification.success({
        message: 'Selamat datang',
        description: 'tpq-elrahman-admin',
        placement: 'bottomRight'
      })
    } else {
      notification.error({
        message: 'Peringatan',
        description: 'Gagal memasuki dashboard, username atau password tidak valid!',
        placement: 'bottomRight'
      })
    }
  }

  return (
    <div className="min-h-[100vh] w-full relative">
      <Head>
        <title>TPQ - Dashboard</title>
      </Head>
      <div className="absolute opacity-10 top-0 left-0 w-full h-full"  style={{ background: 'url("background.jpg")' }}></div>
      <Form className="relative pb-6 pt-20 mb-12 max-w-sm mx-auto px-4" onFinish={onFinish}>
        <div className="w-[10rem] h-[10rem] mx-auto relative">
          <Image src="/tpq.png" alt="tpq" layout="fill" objectFit="contain" />
        </div>
        <div className="text-center mt-3 mb-5">
          <Typography.Title className="mb-0" level={2}>El-Rahmah Dashboard</Typography.Title>
          <Typography.Paragraph>
            Kelola halaman anda dengan lebih mudah
          </Typography.Paragraph>
        </div>
        <Form.Item name="username" rules={[{ required: true, message: 'Please input your username!' }]}>
          <Input size="large" className="my-2 rounded-lg" placeholder="Username" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: 'Please input your password!' }]}>
          <Input.Password size="large" className="my-2 rounded-lg inline-flex" placeholder="Pasword" />
        </Form.Item>
        <Form.Item>
          <Button size="large" className="my-2 rounded-lg w-full" type="primary" htmlType="submit">
            Masuk
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
