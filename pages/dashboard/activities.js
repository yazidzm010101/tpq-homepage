import { useCollection } from "swr-firestore-v9";
import { Button, Typography } from "antd";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight, faPlus } from "@fortawesome/free-solid-svg-icons";
import Image from "next/image";
import DashboardLayout from "../../layouts/DashboardLayout";
import ActivityManager from "../../components/Activity/ActivityManager";
import ActivityCreate from "../../components/Activity/ActivityCreate";
import React from "react";

export default function News() {
  const news = useCollection("activity", {
    orderBy: ["endDate", "desc"],
  });
  const [createShown, setCreateShown ] = React.useState(false);
  return (
    <DashboardLayout title={"TPQ"}>
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/event.png" />
          </div>
          Kegiatan
        </Typography.Title>
        <ActivityManager
          data={news.data}
          loading={news.loading}
          error={news.error}
        />
        <ActivityCreate
          visible={createShown}
          onCancel={() => setCreateShown(false)}
          onSubmit={async (data) => {
            await news.add(data);
            setCreateShown(false);
          }}
        />
        <Button
          type="primary"
          size="large"
          className="mb-3 fixed bottom-10 right-10 shadow-lg"
          onClick={() => setCreateShown(true)}
          icon={<FontAwesomeIcon className="h-3 w-auto inline-block mb-1 mr-2" icon={ faPlus }/>}
        >
          Tambah
        </Button>
      </div>
    </DashboardLayout>
  );
}
