import { Button, Typography } from "antd";
import DashboardLayout from "../../layouts/DashboardLayout";
import { useCollection } from "swr-firestore-v9";
import Image from "next/image";
import GalleryCreate from "../../components/Gallery/GalleryCreate";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import SubjectManager from "../../components/Subjects/SubjectsManager";
import SubjectCreate from "../../components/Subjects/SubjectsCreate";

export default function LearningCenter() {  
  const subject = useCollection('learning_center')
  const [ createShown, setCreateShown ] = React.useState(false)
  return (
    <DashboardLayout title={"TPQ"}>         
      <div className="py-6 mb-12 max-w-7xl mx-auto px-4">
        <Typography.Title level={2} className="pt-16 pb-8">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image
              layout="fill"
              objectFit="contain"
              src="/book.png"              
            />
          </div>
          E-Learning
        </Typography.Title>
        <SubjectManager data={subject.data} loading={subject.loading} error={subject.error}/>        
        <SubjectCreate
          visible={createShown}         
          onSubmit={async (data) => await subject.add(data)} 
          onCancel={() => setCreateShown(false)}
        />
        <Button
          type="primary"
          size="large"
          className="mb-3 fixed bottom-10 right-10 shadow-lg"
          onClick={() => setCreateShown(true)}
          icon={<FontAwesomeIcon className="h-3 w-auto inline-block mb-1 mr-2" icon={ faPlus }/>}
        >
          Tambah
        </Button>
      </div>
    </DashboardLayout>
  );
}
