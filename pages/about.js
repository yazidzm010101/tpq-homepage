import { useCollection } from "swr-firestore-v9";
import HomeLayout from "../layouts/HomeLayout";
import Jumbotron from "../components/Jumbotron";
import Image from "next/image";
import { Col, Row, Typography } from "antd";

export default function About() {
  const activities = useCollection("activity");
  const gallery = useCollection("gallery");
  console.log(activities.data);
  console.log(gallery.data);
  return (
    <HomeLayout title={"TPQ"}>
      <div
        className="bg-gray-100 py-12 mb-6 relative"
        style={{ background: 'url("background.jpg")' }}
      >
        <div className="h-full w-full bottom-0 left-0 absolute bg-gray-50 opacity-70"></div>        
        <div className="py-6 max-w-5xl mx-auto px-4 relative">
          <div className="text-center">
            <Typography.Paragraph className="mb-0">
              Taman Pendidikan Al-Qur&apos;an
            </Typography.Paragraph>
            <Typography.Title level={2} className="mt-0">El-Rahmah</Typography.Title>
          </div>
          <div className="w-[9rem] h-[9rem] mx-auto relative">
            <Image src="/tpq.png" alt="tpq" layout="fill" objectFit="contain" />
          </div>
          <div className="text-center">
            <Typography.Title level={5} className="mb-0 mt-2 leading-relaxed font-normal">
              Menanamkan pemahaman sesuai dengan Al-Quran dan As-Sunnah<br/>
                Menyelenggarakan pendidikan dan pelajaran Al-Quran<br/>
                Meningkatkan kompetensi di bidang pelajaran agama Islam              
            </Typography.Title>
          </div>
        </div>        
      </div>
      <div className="py-6 max-w-5xl mx-auto px-4">
        <Typography.Title level={2} className="mb-2">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/tower.png" />
          </div>
          Sejarah
        </Typography.Title>
        <Typography.Paragraph className="leading-relaxed">
          Taman Pendidikan Al-Qur’an (TPQ) sangat berperan dalam melahirkan
          generasi muslim yang cinta dan mengamalkan syariat Islam yang
          diimplementasikan dalam kehidupan berbangsa dan bernegara serta mampu
          menghadapi tantangan di era globalisasi. Tujuan TPQ untuk menyiapkan
          terbentuknya generasi Qurani, yaitu generasi yang memiliki komitmen
          terhadap al-Quran sebagai sumber perilaku, pijakan hidup dan rujukan
          segala urusan. Hal itu ditandai dengan kecintaan yang mendalam
          terhadap Al-Qur’an, mampu dan rajin membacanya, terus menerus
          mempelajari isi kandungannya, serta memiliki kemauan yang kuat untuk
          mengamalkannya secara kaffah dalam kehidupan sehari-hari. TPQ Elrahmah
          berdiri sejak Oktober 2015 sebagai jawaban terhadap tantangan dan
          kebutuhan masyarakat akan pendidikan agama bagi anak-anak usia sekolah
          sedini mungkin di lingkungan Musala Al-Ikhlas.
        </Typography.Paragraph>
      </div>
      {/* <div className="py-6 mb-10 max-w-5xl mx-auto px-4">
        <Typography.Title level={2} className="mb-2">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/goal.png" />
          </div>
          Visi
        </Typography.Title>
        <Typography.Paragraph className="leading-relaxed">
          Menjadikan Santri yang Saleh dan Berakhlakul Karimah (SARAH)
        </Typography.Paragraph>
      </div>
      <div className="py-6 mb-10 max-w-5xl mx-auto px-4">
        <Typography.Title level={2} className="mb-2">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/todo.png" />
          </div>
          Misi
        </Typography.Title>
        <Typography.Paragraph className="leading-relaxed">
          <ol>
            <li>Menanamkan pemahaman sesuai dengan Al-Quran dan As-Sunnah</li>
            <li>Menyelenggarakan pendidikan dan pelajaran Al-Quran</li>
            <li>Meningkatkan kompetensi di bidang pelajaran agama Islam</li>
          </ol>
        </Typography.Paragraph>
      </div> */}
      <div className="py-6 max-w-5xl mx-auto px-4">
        <Typography.Title level={2} className="mb-2">
          <div className="h-16 w-16 relative inline-block align-middle mr-4">
            <Image layout="fill" objectFit="contain" src="/people.png" />
          </div>
          Pengelola
        </Typography.Title>
        <Typography.Paragraph className="leading-relaxed">
          TPQ Elrahmah hadir dari, oleh, dan untuk masyarakat di Kelurahan
          Pondok Cabe Udik, Pamulang, Kota Tangerang Selatan dan masyarakat
          lainnya tanpa mengenal batas wilayah. Untuk pengelola dan asatizah
          Kegiatan Belajar Mengajar (KMB) berasal dari potensi masyarakat dan
          aktivis dakwah yang sudah lama berkecimpung di dunia pendidikan.
        </Typography.Paragraph>
      </div>
    </HomeLayout>
  );
}
