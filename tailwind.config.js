module.exports = {
  important: true,
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./layouts/**/*.{js,ts,jsx,tsx}",
  ],
  extend: {
    screens: {
      xs: "360px",
    },
  },
  plugins: [require("tailwind-children")]
};
