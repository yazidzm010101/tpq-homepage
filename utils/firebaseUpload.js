import { getStorage, ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { getFuego } from "swr-firestore-v9";

export default function firebaseUploader(path) {
  return function ({ onSuccess, onProgress, onError, file }) {            
    const app = getFuego().db.app    
    const storage = getStorage(app);
    const fileName = new Date().getTime() + file.name;
    const storageRef = ref(storage, [path, fileName].join("/"));  
    const task = uploadBytesResumable(storageRef, file)      
    task.on(
      "state_changed",
      function progress(snapshot) {
        onProgress(
          Math.floor(snapshot.bytesTransferred / snapshot.totalBytes).toFixed(
            2
          ) * 100,
          file
        );
      },
      function error(err) {
        onError(err, file);
      },
      function complete() {
        getDownloadURL(task.snapshot.ref).then((fileUrl) => {
          onSuccess(fileUrl, file);
        });
      }
    );
  };
}
