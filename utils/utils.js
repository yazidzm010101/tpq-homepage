export function dateToString(text, isRelative=false) {
  var currentDate = new Date();
  var date = new Date(text);
  var secondDiff = (currentDate.getTime() - date.getTime()) / 1000;
  if (isRelative) {
    if (secondDiff / 60 < 60) {
      return ~~(secondDiff / 60) + " menit yang lalu";
    }
    if (secondDiff / 3600 < 24) {
      return ~~(secondDiff / 3600) + " jam yang lalu";
    }
    if (secondDiff / (3600 * 24) <= 15) {
      return ~~(secondDiff / (3600 * 24)) + " hari yang lalu";
    }
  }
  return date.toLocaleDateString("ID", { dateStyle: "long" });
}

export function charToColor(char = "A") {
  char = char.toUpperCase();
  const range = 25;
  const code = char.charCodeAt(0) - 65;
  const h = 336 * (code / range);
  const s = 36;
  const l = 36;
  const a = 1;
  return `hsla(${h}, ${s}%, ${l}%, ${a})`;
}

export function chartToPatternStyle(char = "A") {
  char = char.toUpperCase();
  const range = 25;
  var charCode = char.charCodeAt(0);
  if (charCode >= 65) {
    charCode -= 65;
  } else {
    charCode = 65 - charCode;
  }

  const styles = [
    // wavy
    {"backgroundColor":"#e5e5f7","opacity":"0.05","backgroundImage":"repeating-radial-gradient( circle at 0 0, transparent 0, #e5e5f7 10px ), repeating-linear-gradient( #00000055, #000000 )"},
    // rhombus
    {"backgroundColor":"#e5e5f7","opacity":"0.05","backgroundImage":"linear-gradient(135deg, #000000 25%, transparent 25%), linear-gradient(225deg, #000000 25%, transparent 25%), linear-gradient(45deg, #000000 25%, transparent 25%), linear-gradient(315deg, #000000 25%, #e5e5f7 25%)","backgroundPosition":"10px 0, 10px 0, 0 0, 0 0","backgroundSize":"10px 10px","backgroundRepeat":"repeat"},
    // zigzag
    {"backgroundColor":"#e5e5f7","opacity":"0.05","backgroundImage":"linear-gradient(135deg, #000000 25%, transparent 25%), linear-gradient(225deg, #000000 25%, transparent 25%), linear-gradient(45deg, #000000 25%, transparent 25%), linear-gradient(315deg, #000000 25%, #e5e5f7 25%)","backgroundPosition":"10px 0, 10px 0, 0 0, 0 0","backgroundSize":"20px 20px","backgroundRepeat":"repeat"},
    // zigzag 3d
    {"backgroundColor":"#e5e5f7","opacity":"0.05","background":"linear-gradient(135deg, #00000055 25%, transparent 25%) -10px 0/ 20px 20px, linear-gradient(225deg, #000000 25%, transparent 25%) -10px 0/ 20px 20px, linear-gradient(315deg, #00000055 25%, transparent 25%) 0px 0/ 20px 20px, linear-gradient(45deg, #000000 25%, #e5e5f7 25%) 0px 0/ 20px 20px"},
    // moon
    {"backgroundColor":"#e5e5f7","opacity":"0.05","backgroundImage":"radial-gradient( ellipse farthest-corner at 10px 10px , #000000, #000000 50%, #e5e5f7 50%)","backgroundSize":"10px 10px"},
    // circles
    {"backgroundColor":"#e5e5f7","opacity":"0.05","backgroundImage":"radial-gradient(circle at center center, #000000, #e5e5f7), repeating-radial-gradient(circle at center center, #000000, #000000, 10px, transparent 20px, transparent 10px)","backgroundBlendMode":"multiply"},
  ];

  return styles[charCode % styles.length]
}
