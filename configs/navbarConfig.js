import {
  faHouse,
  faGraduationCap,
  faNewspaper,
  faImages,
  faSchoolFlag,
  faHome,
  faBook,
  faInfo,
} from "@fortawesome/free-solid-svg-icons";
import { Image } from "antd";

const navbarConfig = {
  data: [
    { label: "Beranda", faIcon: faHome, href: "/", isStrict: true },
    { label: "E-Learning", faIcon: faBook , href: "/learning-center", isStrict: false },
    {
      label: "Berita",
      faIcon: faNewspaper,
      isStrict: false,
      submenu: [
        { label: "Berita", href: "/news" },
        { label: "Kegiatan", href: "/activities" },
      ],
    },
    { label: "Galeri", faIcon: faImages, href: "/gallery", isStrict: false },
    { label: "Tentang", faIcon: faInfo, href: "/about", isStrict: false },
  ],
  logo: "/tpq.png",
};

export default navbarConfig;
