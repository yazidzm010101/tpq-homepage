import {
  faHouse,
  faGraduationCap,
  faNewspaper,
  faImages,
  faSchoolFlag,
  faCalendar,
} from "@fortawesome/free-solid-svg-icons";
import { Image } from "antd";

const navbarConfig = {
  data: [
    {
      label: "E-Learning",
      faIcon: faGraduationCap,
      href: "/dashboard/learning-center",
      isStrict: false,
    },
    {
      label: "Berita",
      faIcon: faNewspaper,
      href: "/dashboard/news",
      isStrict: false,
    },
    {
      label: "Kegiatan",
      faIcon: faCalendar,
      href: "/dashboard/activities",
      isStrict: false,
    },
    {
      label: "Galeri",
      faIcon: faImages,
      href: "/dashboard/gallery",
      isStrict: false,
    },
  ],
  logo: "/tpq.png",
};

export default navbarConfig;
